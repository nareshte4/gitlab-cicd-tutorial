var request = require("request");

var base_url = "http://localhost:5900/"

describe("Hello Welcome page", function() {
  describe("GET /", function() {
    it("returns status code 400", function(done) {
      request.get(base_url, function(error, response, body) {
        expect(response.statusCode).toBe(400);
        done();
      });
    });
  });
});